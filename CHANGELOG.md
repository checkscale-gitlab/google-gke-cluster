# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.6] - 2020-01-12
### Fixed
- missing default value for `nodepool_nodes_per_zone` variable

## [0.1.5] - 2020-01-12
### Added
- f80df6d2 support for Workload Identity
- de85fb6a lifecycle control to ignore `node_pool` and `master_auth` changes

### Changed
- 8dfbb528 default `initial_node_count` for Nodepool is `1` if not set
- update README with latest variables

### Fixed
- 222d30c6 `google-beta` provider syntax deprecation for Terraform >= v0.12.19

## [0.1.4] - 2019-11-19
### Removed
- deprecated Kubernetes Dashboard addon that was removed for clusters on GKE
- `use_ip_aliases` field removed from the resource since 3.0.0

## [0.1.3] - 2019-10-10
### Added
- GKE Nodepool configuration after the GKE Cluster is initialized; they are dependent on each
  other

## [0.1.3-rc] - 2019-09-09
### Fixed
- 8d16a3d4 output value of GKE Cluster name that was just pointing to a variable instead of the
  actual Terraform computed resource; this way the Nodepool will actually wait for the GKE Cluster
  to be created first

## [0.1.2] - 2019-09-08
### Fixed
- broken `map` that was holding autoscaling resource limits

## [0.1.1] - 2019-09-08
### Fixed
- syntax error when injecting the autoscaling resource limits
- broken syntax in README

## [0.1.0] - 2019-09-08
### Added
- 2786407b ability to tweak the autoscaling resource limits
- 579f5422 proper module documentation

### Fixed
- 98c09102 undocumented fields from readme

## [0.0.1] - 2019-09-07
### Added
- CHANGELOG + LICENCE + README

[Unreleased]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-cluster/compare/v0.1.6...master
[0.1.6]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-cluster/compare/v0.1.5...v0.1.6
[0.1.5]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-cluster/compare/v0.1.4...v0.1.5
[0.1.4]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-cluster/compare/v0.1.3...v0.1.4
[0.1.3]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-cluster/compare/v0.1.3-rc...v0.1.3
[0.1.3-rc]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-cluster/compare/v0.1.2...v0.1.3-rc
[0.1.2]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-cluster/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-cluster/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-cluster/compare/v0.0.1...v0.1.0
[0.0.1]: https://gitlab.com/WizDevOps/terraform-modules/google-gke-cluster/tree/v0.0.1