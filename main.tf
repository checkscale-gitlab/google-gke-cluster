resource "google_container_cluster" "default" {
  provider = google-beta

  name     = coalesce(var.cluster_name, "foobar-cluster")
  location = var.region
  project  = var.project_id

  remove_default_node_pool = true
  initial_node_count       = 1

  monitoring_service = var.monitoring_service
  logging_service    = var.logging_service

  network_policy {
    enabled  = var.network_policy
    provider = var.network_policy_provider
  }

  node_config {
    service_account = google_service_account.cluster_service_account.email
    machine_type    = var.machine_type
    preemptible     = var.preemptible_node_pool
    disk_type       = var.node_config_disk_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    workload_metadata_config {
      node_metadata = var.node_metadata
    }

    labels       = var.labels
    tags         = var.tags
    oauth_scopes = var.oauth_scopes
  }

  resource_labels = var.labels

  # Setting an empty username and password explicitly disables basic auth
  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  timeouts {
    create = "30m"
    update = "40m"
  }

  addons_config {
    http_load_balancing {
      disabled = false
    }

    horizontal_pod_autoscaling {
      disabled = false
    }

    istio_config {
      disabled = var.istio_disabled
      auth     = var.istio_auth
    }

    network_policy_config {
      disabled = false
    }
  }

  lifecycle {
    ignore_changes = [master_auth, node_pool]
  }

  vertical_pod_autoscaling {
    //      https://cloud.google.com/kubernetes-engine/docs/concepts/verticalpodautoscaler
    enabled = false
  }

  maintenance_policy {
    daily_maintenance_window {
      start_time = var.maintenance_start_time
    }
  }

  network    = var.network
  subnetwork = var.subnetwork

  workload_identity_config {
    identity_namespace = "${var.project_id}.svc.id.goog"
  }

  ip_allocation_policy {
    cluster_secondary_range_name  = var.cluster_secondary_range_name
    services_secondary_range_name = var.cluster_secondary_range_name
  }

  private_cluster_config {
    enable_private_endpoint = var.disable_public_endpoint
    enable_private_nodes    = var.enable_private_nodes
    master_ipv4_cidr_block  = var.master_ipv4_cidr_block
  }

  dynamic "master_authorized_networks_config" {
    for_each = var.master_authorized_networks_config
    content {
      dynamic "cidr_blocks" {
        for_each = lookup(master_authorized_networks_config.value, "cidr_blocks", [])
        content {
          cidr_block   = cidr_blocks.value.cidr_block
          display_name = lookup(cidr_blocks.value, "display_name", null)
        }
      }
    }
  }
}

resource "google_container_node_pool" "default" {
  provider           = google-beta
  name               = google_container_cluster.default.name
  location           = var.region
  project            = var.project_id
  cluster            = google_container_cluster.default.name
  initial_node_count = var.nodepool_nodes_per_zone

  autoscaling {
    min_node_count = var.nodepool_autoscaling_nodes_count.min
    max_node_count = var.nodepool_autoscaling_nodes_count.max
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  node_config {
    preemptible     = true
    machine_type    = var.machine_type
    image_type      = "COS"
    service_account = google_service_account.cluster_service_account.email

    metadata = {
      disable-legacy-endpoints = "true"
    }

    labels       = var.labels
    tags         = var.tags
    oauth_scopes = var.oauth_scopes
  }

  lifecycle {
    ignore_changes = [initial_node_count]
  }

  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
  }
}
