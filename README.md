# GCP GKE Cluster
[![changelog](https://img.shields.io/badge/changelog-Keep%20a%20Changelog%20v1.1.0-orange)](https://gitlab.com/WizDevOps/terraform-modules/google-gke-cluster/blob/master/CHANGELOG.md)
[![telegram](https://img.shields.io/badge/chat-kubernetes_en-blue?style=flat&logo=telegram)](https://t.me/kubernetes_en)

Spins up a Google Kubernetes Engine (GKE) Cluster

## Required variables

| Variable | Type | Description | Example |
| -------- | ---- | ----------- | ------- |
| `project_id` | `string` | GCP Project ID | `sample-project123` |
| `region` | `string` | The Region of your GCP Project | `europe-west4` |
| `cluster_name` | `string` | Name of GKE Cluster | `sample-cluster` |
| `network_policy` | `bool` | Whether Network Policy is enabled on the cluster | `false` |
| `machine_type` | `string` | Size of [your instances][3] | `f1-micro` |
| `preemptible_node_pool` | `bool` | Toggle preemptible instances | `true` |
| `labels` | `map` | Label your cluster accordingly | `{env = "staging"}` |
| `tags` | `list` | Tag your cluster accordingly | `["dev"]` |
| `oauth_scopes` | `list` | Grant some permissions to your cluster | `["https://www.googleapis.com/auth/compute",]`|
| `network` | `string` | A reference to the VPC Network | `module.vpc_network.network` |
| `subnetwork` | `string` | Reference to the Subnetwork | `module.vpc_network.subnetwork` |
| `cluster_secondary_range_name` | `string` | Secondary CIDR range for the Cluster | `module.vpc_network.public_subnetwork_secondary_range_name` |
| `enable_private_nodes` | `bool` | Nodes have internal IP addresses only | `false` |
| `master_ipv4_cidr_block` | `string` | CIDR range for the master to use | `"10.3.0.0/28"` |
| `master_authorized_networks_config` | `list` | Permit/Deny master external access | `[{}]` |

## Optional variables

| Variable | Type | Description | Example |
| -------- | ---- | ----------- | ------- |
| `monitoring_service` | `string` | Available options on [Terraform docs][1] | `none` |
| `logging_service` | `string` | Available options on [Terraform docs][2] | `none` |
| `network_policy_provider` | `string` | Selected network policy provider | `CALICO` |
| `node_metadata` | `string` | Expose Node metadata to the workload running on the node | `GKE_METADATA_SERVER` |
| `node_config_disk_type` | `string` | Type of Disk to attach on VMs (SSD/HDD) | `pd-standard` |
| `istio_disabled` | `bool` | Toggle Istio mesh network | `true` |
| `istio_auth` | `string` | Authentication types between services in Istio | `AUTH_MUTUAL_TLS` or `AUTH_NONE` |
| `maintenance_start_time` | `string` | Set a maintenance time for GCP | `03:00` |
| `disable_public_endpoint` | `bool` | Lock access to the Master by assigning Private IPv4 Address | `false` |
| `nodepool_nodes_per_zone` | `number` | Control how many nodes you want per AZ | `1` |
| `nodepool_autoscaling_nodes_count` | `map(string)` | Nodepool autoscaling limits | `{min=5, max=20}` |

## Outputs

| Variable | Description |
| -------- | ----------- |
| `gke_cluster_name` | Name of the GKE Cluster created |
| `cluster_service_account_email` | Email of the GKE ServiceAccount |
| `cluster_service_account_name` | Name of the GKE ServiceAccount |


## Usage

```hcl
module "gke_cluster" {
  source = "git::https://gitlab.com/WizDevOps/terraform-modules/google-gke-cluster.git?ref=v0.1.5"

  project_id                   = local.project_id
  region                       = local.region
  cluster_name                 = "sample-cluster"
  network_policy               = true
  machine_type                 = "f1-micro"
  preemptible_node_pool        = true
  node_config_disk_type        = "pd-standard"
  network                      = module.vpc_network.network
  subnetwork                   = module.vpc_network.public_subnetwork
  master_ipv4_cidr_block       = "10.3.0.0/28"
  enable_private_nodes         = true
  cluster_secondary_range_name = module.vpc_network.public_subnetwork_secondary_range_name
  labels = {
    environment     = "dev"
    cluster-version = "1.0.0"
  }
  tags = ["dev"]
  oauth_scopes = [
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/devstorage.read_only",
    "https://www.googleapis.com/auth/ndev.clouddns.readwrite",
  ]
  master_authorized_networks_config = [
    {
      cidr_blocks = [
        {
          cidr_block   = "0.0.0.0/0"
          display_name = "can-access-kubernetes-master"
        },
      ]
    },
  ]
}
```

[1]: https://www.terraform.io/docs/providers/google/r/container_cluster.html#monitoring_service
[2]: https://www.terraform.io/docs/providers/google/r/container_cluster.html#logging_service
[3]: https://cloud.google.com/compute/docs/machine-types